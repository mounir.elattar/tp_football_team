package com.example.tp3;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Parcelable;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    private SportDbHelper sportDbHelper;
    private  SimpleCursorAdapter adapter;
    private SwipeRefreshLayout swiperefresh;
    private SwipeDetector swipeDetector = new SwipeDetector();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        sportDbHelper = new SportDbHelper(this);
        SQLiteDatabase db = sportDbHelper.getReadableDatabase();
        sportDbHelper.populateOnlyOneTime(db);

        final Cursor cursor = sportDbHelper.fetchAllTeams();
        adapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2, cursor,
                new String[] {sportDbHelper.COLUMN_TEAM_NAME, sportDbHelper.COLUMN_LEAGUE_NAME},
                new int[] { android.R.id.text1, android.R.id.text2}, 0);

        ListView teamList = (ListView) findViewById(R.id.listTeams);
        swiperefresh = findViewById(R.id.swiperefresh);
        teamList.setAdapter(adapter);
        db.close();

        teamList.setOnTouchListener(swipeDetector);
        teamList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View v, int position, long id) {

                if(swipeDetector.swipeDetected()) {
                    if((swipeDetector.getAction() == Action.RL) ||(swipeDetector.getAction() == Action.LR)) {
                        Cursor cursorTeam =  (Cursor) parent.getItemAtPosition(position);
                        Team teamToDelete = sportDbHelper.cursorToTeam(cursorTeam);
                        sportDbHelper.deleteTeam(teamToDelete.getId());

                        adapter.changeCursor(sportDbHelper.getReadableDatabase().query(SportDbHelper.TABLE_NAME,
                                null, null, null, null, null, null));

                        adapter.notifyDataSetChanged();
                        mt("Team " + teamToDelete.getName() + " has been deleted ");
                    }
                }else{
                    Cursor cursorTeam =  (Cursor) parent.getItemAtPosition(position);

                    Team clickedTeam = sportDbHelper.cursorToTeam(cursorTeam);

                    Intent intent = new Intent(MainActivity.this, TeamActivity.class);
                    intent.putExtra(Team.TAG, (Parcelable) clickedTeam);
                    startActivityForResult(intent,1);
                }

            }
        });

        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new UpdateTask().execute();
            }
        });
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NewTeamActivity.class);
                startActivityForResult(intent,2);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null){
            Team team = data.getParcelableExtra(Team.TAG);
            switch (requestCode){
                case 1 :
                    sportDbHelper.updateTeam(team);
                    mt("Team " + team.getName() + " has been updated ");
                    break;
                case 2 :
                    sportDbHelper.addTeam(team);
                    mt("Team " + team.getName() + " has been added ");
                    break;
            }
            adapter.changeCursor(sportDbHelper.getReadableDatabase().query(SportDbHelper.TABLE_NAME,
                    null, null, null, null, null, null));

            adapter.notifyDataSetChanged();
        }

    }

    public void mt(String string){
        Toast.makeText(this,string,Toast.LENGTH_SHORT).show();
    }

    public class UpdateTask extends AsyncTask<TextView, String, List<Team>> {

        @Override
        protected List<Team> doInBackground(TextView... textViews) {
            List<Team> allTeams = sportDbHelper.getAllTeams();

            for (Team team : allTeams) {
                boolean run = true;
                while (run) {
                    URL url = null;
                    HttpURLConnection urlConnection = null;
                    Match lastEvent = new Match();
                    try {
                        url = WebServiceUrl.buildSearchTeam(team.getName());
                        urlConnection = (HttpsURLConnection) url.openConnection();
                        JSONResponseHandlerTeam jsrTeam = new JSONResponseHandlerTeam(team);
                        try {
                            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                            jsrTeam.readJsonStream(in);
                        } finally {
                            if (!jsrTeam.verifier) {
                                break;
                            }
                            urlConnection.disconnect();
                        }

                        url = WebServiceUrl.buildGetRanking(team.getIdLeague());
                        urlConnection = (HttpsURLConnection) url.openConnection();
                        try {
                            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                            JSONResponseHandlerL jsrLeague = new JSONResponseHandlerL(team);
                            jsrLeague.readJsonStream(in);
                        } finally {
                            urlConnection.disconnect();
                        }

                        url = WebServiceUrl.buildSearchLastEvents(team.getIdTeam());
                        urlConnection = (HttpsURLConnection) url.openConnection();
                        try {
                            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                            JSONResponseHandlerE jsrEvent = new JSONResponseHandlerE(lastEvent);
                            jsrEvent.readJsonStream(in);
                            team.setLastEvent(lastEvent);
                        } finally {
                            urlConnection.disconnect();
                            run = false;
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            return allTeams;
        }

        @Override
        protected void onPostExecute(List<Team> allTeams) {
            super.onPostExecute(allTeams);
            for (Team team : allTeams) {
                sportDbHelper.updateTeam(team);
            }
            adapter.changeCursor(sportDbHelper.getReadableDatabase().query(SportDbHelper.TABLE_NAME,
                    null, null, null, null, null, null));

            adapter.notifyDataSetChanged();
            swiperefresh.setRefreshing(false);
        }

    }

    public static enum Action {
        LR,
        RL,
        None
    }

    public class SwipeDetector implements View.OnTouchListener {

        private static final String logTag = "SwipeDetector";
        private static final int min_dis = 100;
        private float dx, ux, uy;
        private Action mSwiDete = Action.None;

        public boolean swipeDetected() {
            return mSwiDete != Action.None;
        }

        public Action getAction() {
            return mSwiDete;
        }

        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    dx = event.getX();
                    mSwiDete = Action.None;
                    return false; // allow other events like Click to be processed
                }
                case MotionEvent.ACTION_MOVE: {
                    ux = event.getX();
                    uy = event.getY();

                    float deltaX = dx - ux;

                    // horizontal swipe detection
                    if (Math.abs(deltaX) > min_dis) {
                        // left or right
                        if (deltaX < 0) {
                            mSwiDete = Action.LR;
                            return true;
                        }
                        if (deltaX > 0) {
                            mSwiDete = Action.RL;
                            return true;
                        }
                    }
                }
            }
            return false;
        }
    }

}

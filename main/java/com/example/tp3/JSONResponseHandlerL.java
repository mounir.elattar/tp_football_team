package com.example.tp3;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class JSONResponseHandlerL {
    private static final String TAG = JSONResponseHandlerL.class.getSimpleName();

    private Team team;


    public JSONResponseHandlerL(Team team) {
        this.team = team;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readTeams(reader);
        } finally {
            reader.close();
        }
    }

    public void readTeams(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("table")) {
                readArrayTeams(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }
    
    private void readArrayTeams(JsonReader reader) throws IOException {
        String teamName = null;
        boolean found = false;
        reader.beginArray();
        int nb = 1;
        while (reader.hasNext() ) {
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (name.equals("name")) {
                    teamName = reader.nextString();
                    if (teamName.equals(team.getName())){
                        found = true;
                        team.setRanking(nb);
                    }
                }else if (name.equals("total") && (found)){
                    team.setTotalPoints(reader.nextInt());
                } else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            nb++;
        }
        reader.endArray();

    }
}
